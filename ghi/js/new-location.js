function createStateDropDownHTML(list){
    let html = '<option selected value="">Choose a state</option>\n'
    for (let state of list){
        html += `<option value=${Object.values(state)}>${Object.keys(state)}</option>\n`
    }
    return html
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/"
    try {
        const response = await fetch(url)
        if (!response.ok){
            console.log("response error")
        } else {
            const data = await response.json()
            const statesHtml = createStateDropDownHTML(data.states)
            const states = document.getElementById('state')
            states.innerHTML += statesHtml
        }

    } catch(e) {
        console.log("fetch error")
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag)
    const json = JSON.stringify(Object.fromEntries(formData))

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
    'Content-Type': 'application/json',
  },
};
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
}
  });
})
