function createCard(name, location, description, pictureUrl, startsOn, endsOn) {
    return `
      <div class="col">
        <div class="card shadow-sm p-3 mb-1 bg-body-tertiary rounded">
          <img src="${pictureUrl}" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          ${startsOn.getMonth()}/${startsOn.getDate()}/${startsOn.getFullYear()} - ${endsOn.getMonth()}/${endsOn.getDate()}/${endsOn.getFullYear()}
          </div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/'

try {
    const response = await fetch(url)

    if (!response.ok) {
        const responseErrorHTML = `<div class ="alert alert-danger" role="alert">Oops something went wrong, error code is ${response.status}</div>`
        const responseErrorTag = document.querySelector('body')
        responseErrorTag.innerHTML = responseErrorHTML
    } else {
        const data = await response.json()

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const location = details.conference.location.name
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startsOn = new Date(details.conference.starts)
              const endsOn = new Date(details.conference.ends)
              const html = createCard(title, location, description, pictureUrl,startsOn,endsOn);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }

        }
      } catch (e) {
        const responseCatchErrorHTML = `<div class ="alert alert-danger" role="alert">Oops something went wrong when attempting to fetch >:)</div>`
        const responseCatchErrorTag = document.querySelector('body')
        responseCatchErrorTag.innerHTML = responseCatchErrorHTML
}

});
