function createLocationDropDownHTML(list){
    let html = '<option selected value="">Choose a location</option>\n'
    for (let location of list){
        html += `<option value=${location.id}>${location.name}</option>\n`
    }
    return html
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/"
    try {
        const response = await fetch(url)
        if (!response.ok){
            console.log("response error")
        } else {
            const data = await response.json()
            const html = createLocationDropDownHTML(data.locations)
            const locations = document.getElementById('location')
            locations.innerHTML += html
        }

    } catch(e) {
        console.log("fetch error")
    }
    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceURL = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
          },
        }
        const response = await fetch(conferenceURL,fetchConfig)
        if (response.ok){
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference)
        }
    })
})
