window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    }
    const formTag = document.getElementById('create-presentation-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const conferenceValue = formData.get("conference")
        const json = JSON.stringify(Object.fromEntries(formData))
        const parsedJson = JSON.parse(json)
        const conf_id = parsedJson.conference.replace("/api/conferences/","").replace("/","")
        delete parsedJson.conference
        parsedJson.status = "SUBMITTED"
        const newJson = JSON.stringify(parsedJson)
        const presentationURL = `http://localhost:8000/api/conferences/${conf_id}/presentations/`
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }


        const response = await fetch(presentationURL,fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newPresentation = await response.json()
            console.log(newPresentation)

        }
    })
})
